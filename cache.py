#!/usr/bin/env python3


from murmurhash3 import m_hash
import utils


class UserspaceSubtable:
    """Class representing subtable in userspace cache.
    """
    def __init__(self, key, mask, cache, max_priority):
        """Creates subtable in userspace cache.

        Args:
            key:          Key of subtable.
            mask:         Mask of subtable.
            cache:        Cache of subtable.
            max_priority: Priority of most prioritized
                          rule in given subtable.
        """
        self.key = key # set()
        self.mask = mask # int
        self.cache = [None]*1024 # list()
        self.max_priority = max_priority # int


class MegaflowSubtable:
    """Class representing subtable in megaflow cache.
    """
    def __init__(self, key, mask, cache):
        """Creates subtable in megaflow cache.

        Args:
            key:   Key of subtable.
            mask:  Mask of subtable.
            cache: Cache of subtable.
        """
        self.key = key # set()
        self.mask = mask # int
        self.cache = [None]*1024 # list()
        self.hits = 1


class MegaflowSubtableLookup:
    """Class representing structure used to find
    index of proper subtable in megaflow cache.
    """
    def __init__(self, key_mask):
        """Creates structure used to find index of
        proper subtable in megaflow cache and sets
        number of hits in this subtable to 1.

        Args:
            key_mask: Set consisting of key of proper
                      subtable and its mask.
        """
        self.key_mask = key_mask # set()
        self.hits = 1


class Cache:
    """Class representing abstract cache
    after which other classes can inherit.
    """
    fields_len = [48, # src_mac
                  48, # dst_mac
                  32, # src_ip
                  32, # dst_ip
                  8,  # protocol
                  16, # src_port
                  16] # dst_port


    def _calc_subfields(self, key, fields):
        """Calculates number to be used as input to hash function.

        Args:
            key:    Key of the subtable.
            fields: Fields of header set in order.
        Returns:
            Number to be used as input to murmurhash function.
        """
        fin_out = 0
        for index in key:
            fin_out = (fin_out << self.fields_len[index]) | fields[index]
        return fin_out


class UserCache(Cache):
    """Class representing userspace cache - a data
    structure used to represent OvS tables efficienty.
    """
    def __init__(self):
        """Creates list of OvS tables and registry to quickly
        find subtables' indices in proper OvS tables.
        """
        self.tables = list()
        self.subtables_registry = list()
        self.sorted = False
        self.fields_track = list() # for lookup purposes only


    def add_flow(self, flow):
        """Adds a flow to userspace cache.

        Args:
            flow: Object representing flow containing
                  information to insert a rule.
        """
        # if it's first subtable (kind of match) in a given OvS table, add OvS table
        if len(self.tables) == flow['ovs_table']:
            self.tables.append([])
            self.subtables_registry.append([])

        key = set() # key of the subtable, by indices: 0 = src_mac, 1 = dst_mac, 2 = src_ip, ...
        f_values = []
        masks = []
        for index, field in enumerate(flow, -1):
            if field not in ['ovs_table', 'action', 'priority']:
                f_values.append(flow[field][0])
                masks.append(flow[field][1])
                if flow[field][1] != 0:
                    key.add(index)
        for_cache = self._calc_subfields(key, f_values) # hash input (but & with mask beforehand)
        mask = self._calc_subfields(key, masks) # mask of the UserspaceSubtable

        # Check if there is a proper subtable.
        # If there is, add entry to it;
        # it there is not, add subtable and add entry to it.
        try:
            # check in table if there is a proper subtable
            _n_subt = self.subtables_registry[flow['ovs_table']].index((key, mask)) # number of proper subtable
            # if the subtable is there, add record to it
            ind = m_hash(for_cache & mask, 1024)
            while True:
                if self.tables[flow['ovs_table']][_n_subt].cache[ind] is not None: # open-addressing with linear probing
                    ind = (ind + 1) % 1024
                else: # insert
                    self.tables[flow['ovs_table']][_n_subt].cache[ind] = (for_cache & mask, flow['priority'], flow['action'])
                    if self.tables[flow['ovs_table']][_n_subt].max_priority < flow['priority']:
                        self.tables[flow['ovs_table']][_n_subt].max_priority = flow['priority']
                    break
        except ValueError:
            # the subtable is not there and must be added
            self.subtables_registry[flow['ovs_table']].append((key, mask))
            self.tables[flow['ovs_table']].append(UserspaceSubtable(key=key, mask=mask, cache=[None]*1024, max_priority=flow['priority']))
            # now, when it is added, add record to the subtable
            ind = m_hash(for_cache & mask, 1024)
            self.tables[flow['ovs_table']][-1].cache[ind] = (for_cache & mask, flow['priority'], flow['action'])


    def _priority_sorting(self):
        """Sorts subtables in all tables by max priority, where
        max priority is priority of most-prioritized rule in subtable.
        """
        for table in self.tables:
            table.sort(key=lambda sub: sub.max_priority, reverse=True)
        self.sorted = True


    def _clear_tracker(self):
        """Clears value containing fields saved in the
        process of clasiffying header in userspace cache.
        """
        self.fields_track = list()


    def _pkt_lookup_in_table(self, table, header):
        """Lookups packet in OvS userspace classification pipeline.

        Args:
            table:  Table in which to look for rule.
            header: Header of packet being processed.
        Returns:
            Output action. In the end it will always be
            number in range 0-9, where 0 means drop (send to
            bucket 0) and other values represent otuput buckets.
        """
        tracker = None # in case of lack of even default entry
        entry_priority = 0
        action = 0 # in case of lack of even default entry
        for subtable in self.tables[table]:
            if self.sorted and subtable.max_priority < entry_priority: # if subtables are priority-sorted, there is no need to look further
                break
            info = utils.subtable_lookup(subtable, header)
            if info is not None and entry_priority <= info[0]: # if two rules have the same priority, the last is taken; it is vital in case of default rule with priority 0
                action = info[1]
                entry_priority = info[0]
                tracker = (subtable.key, subtable.mask)
        if tracker is None:
            raise ValueError('ERROR: No default entry in table!')
        self.fields_track.append(tracker)
        # actions
        if 0 < action < 256:
            return self._pkt_lookup_in_table(table=action, header=header)
        elif -10 < action <= 0:
            return abs(action) # in the end, regardless of number of recursions, output bucket must be returned


class MegaflowCache(Cache):
    """Class representing megaflow cache.
    """
    def __init__(self):
        """Creates megaflow table in which subtables
        will be stored. Also creates a registry used
        to lookup index of proper subtable quickly.
        """
        self.table = list()
        self.subtables_registry = list()


    def add_flow(self, keys_masks, header, action):
        """Adds flow to megaflow cache.

        Args:
            keys_masks: Tuple containing two elements: keys of fields
                        and their masks. All found in userspace cache.
            header:     Whole header of packet being processed.
            action:     Action found in userspace cache.
        """
        keys = keys_masks[0]
        masks = keys_masks[1]
        fin_key = set()
        fin_mask = 0
        for ind, keysets in enumerate(keys):
            for key in keysets:
                fin_key.add(key)
                fin_mask <<= self.fields_len[key]
            fin_mask |= masks[ind]
        # compute index in subtable
        entry_fields = utils.compute_hash_input(fin_key, header)
        ind_in_subtable = m_hash(entry_fields & fin_mask, 1024)
        # try to find proper subtable
        index = next((ind for ind, subtable in enumerate(self.subtables_registry) if subtable.key_mask == (fin_key, fin_mask)), None)
        if index is not None: # proper subtable exists
            while True:
                if self.table[index].cache[ind_in_subtable] is not None: # open-addressing with linear probing
                    ind_in_subtable = (ind_in_subtable + 1) % 1024
                else:
                    self.table[index].cache[ind_in_subtable] = (entry_fields & fin_mask, action)
                    break
        else: # there is no such subtable yet
            self.table.append(MegaflowSubtable(key=fin_key, mask=fin_mask, cache=[None]*1024))
            self.subtables_registry.append(MegaflowSubtableLookup(key_mask=(fin_key, fin_mask)))
            self.table[-1].cache[ind_in_subtable] = (entry_fields & fin_mask, action)


    def pkt_lookup(self, header):
        """Looks for header of packet in megaflow subtables.

        Args:
            header: Header of the packet being processed.
        Returns:
            None in case of miss or action in case of hit.
        """
        for ind, subtable in enumerate(self.table):
            action = utils.subtable_lookup(subtable, header)
            if action is not None:
                action = action[0]
                self.subtables_registry[ind].hits += 1
                self.table[ind].hits += 1
                return action # in megaflow cache there are no priorities - lookup is stopped on first match
        return None


    def sort_by_hits(self):
        """Sorts subtables in megaflow table by number of hits.
        """
        self.table.sort(key=lambda sub: sub.hits, reverse=True)
        self.subtables_registry.sort(key=lambda sub: sub.hits, reverse=True)


class MicroflowCache:
    """Class representing microflow cache.
    """
    def __init__(self):
        """Creates microflow cache table.
        """
        self.cache = [None]*1048576


    def add_flow(self, header, action):
        """Adds flow to userspace cache.

        Args:
            header: Whole header of packet being processed.
            action: Action found in higher cache.
        """
        index = m_hash(header, 1048576)
        while True:
            if self.cache[index] is not None:
                index = (index + 1) % 1048576
            else:
                self.cache[index] = (header, action)
                break


    def pkt_lookup(self, header):
        """Looks for header of packet in userpace cache.

        Args:
            header: Header of packet being processed.
        """
        index = m_hash(header, 1048576)
        while True:
                if self.cache[index] is None:
                    return None
                else:
                    if self.cache[index][0] == header:
                        return self.cache[index][1] # returns action
                    else:
                        index = (index + 1) % 1048576
