#!/usr/bin/env python3


import re
import utils
from collections import OrderedDict
from ranges_to_bits import range_to_input
from cache import UserCache


class Userspace:
    """Class representing "userspace" part of OvS model.
    """
    def __init__(self):
        """Creates table able to contain many OvS tables
        and pre-cache used to populate userspace cache.
        """
        self.ovs_table = list() # for humans to read
        self.pre_cache = list() # input for caching function
        self.cache = UserCache()


    def add_flow(self, ovs_table=0, src_2=None, dst_2=None, src_3=None, dst_3=None,
                 proto=None, src_port=None, dst_port=None, priority=1, action=0):
        """Adds new flow to specified OvS table

        Args:
            ovs_table: Number of OvS table to insert rule into.
            src_2:     Source MAC address with or without mask or wildcarded
                       in formats: 'ab:ab:ab:ab:ab:ab ff:ff:ff:ff:00:00',
                       'ab:ab:ab:ab:ab:ab', or '*'.
            dst_2:     Destination MAC address with or without mask or
                       wildcarded in formats as above.
            src_3:     Source IP address with or without mask or wildcarded
                       in formats: '192.168.0.1', '10.0.0.12/16',
                       '172.16.0.2 255.255.0.0', or '*'.
            dst_3:     Destination IP address with or without mask or
                       wildcarded in formats as above.
            proto:     Protocol number input as string.
            src_port:  Source port or source ports range in formats:
                       '10345', or '1024-17000'.
            dst_port:  Destination port or destination ports range
                       in formats as above.
            priority:  Priority of inserted rule.
            action:    Action to execute when incoming packet matches rule.
                       Actions in range -9 to 255. Negative action means
                       output to proper bucket, 0 means output to drop bucket.
                       1-255 means resubmitting to proper OvS table.
        """
        # OvS table
        if len(self.ovs_table) < ovs_table:
            raise IndexError('Cannot add entries to (N+1)th table if there is no Nth table')
        elif len(self.ovs_table) == ovs_table: # if entry is the first to be inserted into a new OvS table
            self.ovs_table.append([]) # add new OvS table


        # source MAC
        if src_2 is None or src_2 == '*':
            self.pp_src_mac = '*'
            self.cc_src_mac = [0, 0]
        elif re.match(r'^(?:[a-f0-9]{2}:){5}[a-f0-9]{2}$', src_2):
            self.pp_src_mac = src_2 + ' ff:ff:ff:ff:ff:ff'
            self.cc_src_mac = [utils.mac_to_num(src_2), 0xffffffffffff]
        else:
            macs = re.match(r'^((?:[a-f0-9]{2}:){5}[a-f0-9]{2})\s((?:[a-f0-9]{2}:){5}[a-f0-9]{2})$', src_2)
            if macs:
                self.pp_src_mac = src_2
                self.cc_src_mac = [utils.mac_to_num(macs.group(1)), utils.mac_to_num(macs.group(2))]
            else:
                raise TypeError('Bad source MAC format')

        # destination MAC
        if dst_2 is None or dst_2 == '*':
            self.pp_dst_mac = '*'
            self.cc_dst_mac = [0, 0]
        elif re.match(r'^(?:[a-f0-9]{2}:){5}[a-f0-9]{2}$', dst_2):
            self.pp_dst_mac = dst_2 + ' ff:ff:ff:ff:ff:ff'
            self.cc_dst_mac = [utils.mac_to_num(dst_2), 0xffffffffffff]
        else:
            macs = re.match(r'^((?:[a-f0-9]{2}:){5}[a-f0-9]{2})\s((?:[a-f0-9]{2}:){5}[a-f0-9]{2})$', dst_2)
            if macs:
                self.pp_dst_mac = dst_2
                self.cc_dst_mac = [utils.mac_to_num(macs.group(1)), utils.mac_to_num(macs.group(2))]
            else:
                raise TypeError('Bad destination MAC format')

        # source IP
        if src_3 is None or src_3 == '*':
            self.pp_src_ip = '*'
            self.cc_src_ip = [0, 0]
        elif re.match(r'^(?:\d{1,3}\.){3}\d{1,3}$', src_3):
            self.pp_src_ip = src_3 + ' 255.255.255.255'
            self.cc_src_ip = [utils.ip_to_num(src_3), utils.ip_to_num('255.255.255.255')]
        else:
            ips1 = re.match(r'^((?:\d{1,3}\.){3}\d{1,3})\/(\d{1,2})$', src_3)
            ips2 = re.match(r'^((?:\d{1,3}\.){3}\d{1,3})\s((?:\d{1,3}\.){3}\d{1,3})$', src_3)
            if ips1:
                self.pp_src_ip = ips1.group(1) + ' ' + utils.num_to_mask(ips1.group(2))
                self.cc_src_ip = [
                    utils.ip_to_num(ips1.group(1)),
                    utils.ip_to_num(utils.num_to_mask(ips1.group(2)))
                ]
            elif ips2:
                self.pp_src_ip = src_3
                self.cc_src_ip = [
                    utils.ip_to_num(ips2.group(1)),
                    utils.ip_to_num(ips2.group(2))
                ]
            else:
                raise TypeError('Bad source IP format')

        # destination IP
        if dst_3 is None or dst_3 == '*':
            self.pp_dst_ip = '*'
            self.cc_dst_ip = [0, 0]
        elif re.match(r'^(?:\d{1,3}\.){3}\d{1,3}$', dst_3):
            self.pp_dst_ip = dst_3 + ' 255.255.255.255'
            self.cc_dst_ip = [utils.ip_to_num(dst_3), utils.ip_to_num('255.255.255.255')]
        else:
            ips1 = re.match(r'^((?:\d{1,3}\.){3}\d{1,3})\/(\d{1,2})$', dst_3)
            ips2 = re.match(r'^((?:\d{1,3}\.){3}\d{1,3})\s((?:\d{1,3}\.){3}\d{1,3})$', dst_3)
            if ips1:
                self.pp_dst_ip = ips1.group(1) + ' ' + utils.num_to_mask(ips1.group(2))
                self.cc_dst_ip = [
                    utils.ip_to_num(ips1.group(1)),
                    utils.ip_to_num(utils.num_to_mask(ips1.group(2)))
                ]
            elif ips2:
                self.pp_dst_ip = dst_3
                self.cc_dst_ip = [
                    utils.ip_to_num(ips2.group(1)),
                    utils.ip_to_num(ips2.group(2))
                ]
            else:
                raise TypeError('Bad destination IP format')

        # protocol
        if proto is None or proto == '*':
            self.pp_protocol = '*'
            self.cc_protocol = [0, 0]
        elif re.match(r'^\d+$', proto):
            self.pp_protocol = proto
            self.cc_protocol = [int(proto), 255]
        else:
            raise TypeError('Bad protocol format')

        # source port
        if src_port is None or src_port == '*':
            self.pp_src_port = '*'
            self.cc_src_port = [[0, 0]]
        elif re.match(r'^\d+$', src_port):
            self.pp_src_port = src_port
            self.cc_src_port = [[int(src_port), 65535]]
        else:
            ports = re.match(r'^(\d+)-(\d+)$', src_port)
            if ports:
                self.pp_src_port = src_port
                self.cc_src_port = range_to_input(int(ports.group(1)), int(ports.group(2)), bits=16)
            else:
                raise TypeError('Bad source port format')

        # destination port
        if dst_port is None or dst_port == '*':
            self.pp_dst_port = '*'
            self.cc_dst_port = [[0, 0]]
        elif re.match(r'^\d+$', dst_port):
            self.pp_dst_port = dst_port
            self.cc_dst_port = [[int(dst_port), 65535]]
        else:
            ports = re.match(r'^(\d+)-(\d+)$', dst_port)
            if ports:
                self.pp_dst_port = dst_port
                self.cc_dst_port = range_to_input(int(ports.group(1)), int(ports.group(2)), bits=16)
            else:
                raise TypeError('Bad destination port format')

        # priority
        try:
            priority = int(priority)
        except ValueError:
            raise TypeError('Bad priority: {}'.format(priority))

        # action
        # actions are:
        # n=0         drop
        # n=<1,255>   resubmit to n-th table
        # n=<-9,-1>   send to n-th interface/bucket
        try:
            action = int(action)
            if not -10 < action < 256:
                raise TypeError('Unsupported action')
        except ValueError:
            raise TypeError('Unsupported action')

        self.ovs_table[ovs_table].append(OrderedDict(
            src_mac=self.pp_src_mac,
            dst_mac=self.pp_dst_mac,
            src_ip=self.pp_src_ip,
            dst_ip=self.pp_dst_ip,
            proto=self.pp_protocol,
            src_port=self.pp_src_port,
            dst_port=self.pp_dst_port,
            priority=priority,
            action=action
        ))

        for s_port in self.cc_src_port:
            for d_port in self.cc_dst_port:
                self.pre_cache.append(OrderedDict(
                    ovs_table=ovs_table,
                    src_mac=self.cc_src_mac,
                    dst_mac=self.cc_dst_mac,
                    src_ip=self.cc_src_ip,
                    dst_ip=self.cc_dst_ip,
                    proto=self.cc_protocol,
                    src_port=s_port,
                    dst_port=d_port,
                    priority=priority,
                    action=action
                ))


    def populate_userspace_cache(self):
        """Populates userspace cache. Needs to be
        executed after adding all flows for experiment.
        """
        for entry in self.pre_cache:
            self.cache.add_flow(entry)


    def lookup(self, header):
        """Lookups whole userspace cache in search for
        entry matching processed header.

        Args:
            header: Header of packet being processed.
        Returns:
            Tuple containing action and fields used in
            pipeline processing of provided header.
        """
        action = self.cache._pkt_lookup_in_table(table=0, header=header)
        tracked_fields = self.cache.fields_track # tracked fields and masks to populate megaflow cache
        self.cache._clear_tracker() # clear before next lookup
        return action, tracked_fields # second returned value only needed in case of megaflow cache


    def priority_sort_userspace(self):
        """Sorts userspace cache subtables by
        max priority of entries stored in them.
        """
        self.cache._priority_sorting()
