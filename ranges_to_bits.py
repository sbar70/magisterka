#!/usr/bin/env python3


def _single_match(begin, end, bits):
    """Calculates single submatch.

    Args:
        begin: Port from which to begin.
        end:   Port on which this single range ends.
        bits:  On how many bits the operation should be performed.
    Returns:
        (begin, mask, new_beginning) : Tuple containing
        port, mask, and new port from which to begin
        next iteration.
    """
    found = 0
    seeker = 1
    while not found:
        found = begin & seeker
        seeker <<= 1
    else:
        seeker >>= 1

    wildcard = seeker - 1
    while begin + wildcard > end:
        wildcard >>= 1

    mask = (2**bits - 1) ^ wildcard
    new_beginning = begin + wildcard + 1

    return begin, mask, new_beginning


def range_to_input(start, stop, bits=16):
    """Transforms port range to bitwise matches with masks.

    Args:
        start: Port to start with.
        stop:  Port to stop on.
        bits:  On how many bits the operation should be performed.
    Returns:
        A list of pairs (port, mask) representing
        range of ports.
    """
    if start > stop:
        raise ValueError('Bad range')
    if not 0 <= start <= 2**bits - 1:
        raise ValueError('Bad range')
    if not 0 <= stop <= 2**bits - 1:
        raise ValueError('Bad range')

    matches = []
    _start = start
    while _start <= stop:
        port, mask, _start = _single_match(_start, stop, bits=bits)
        matches.append([port, mask])

    return matches
