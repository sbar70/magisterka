#!/usr/bin/env python3


from murmurhash3 import m_hash


def num_to_mask(mask):
    """From number computes a human-friendly IPv4 mask in format 255.255.0.0

    Args:
        mask: Numerical value of the mask.
    Returns:
        Mask in human-readable format.
    """
    mask = int(mask)
    full_octets = mask // 8
    half_octet_num = mask % 8
    empty_octets = 4 - full_octets
    if half_octet_num:
        empty_octets -= 1
    ret_mask = ''
    for _ in range(full_octets):
        ret_mask += '.255'
    if half_octet_num:
        ret_mask += str('.' + str(int('1'*half_octet_num + '0'*(8 - half_octet_num), 2)))
    for _ in range(empty_octets):
        ret_mask += '.0'
    return ret_mask.lstrip('.')


def mac_to_num(mac):
    """Transforms human-readable MAC address to number.

    Args:
        mac: MAC address in format ab:ab:ab:ab:ab:ab.
    Returns:
        Numerical representation of MAC address.
    """
    return int(mac.replace(':', ''), 16)


def ip_to_num(ip):
    """Transforms human-readable IP address to number.

    Args:
        ip : IPv4 address in format 192.168.0.1
    Returns:
        Numerical representation of IP address.
    """
    octets = ip.split('.')
    num_ip = 0
    for i, octet in enumerate(octets[::-1], start=0):
        num_ip += (int(octet)*256**i)
    return num_ip


def compute_hash_input(key, header):
    """Computes number to be input to hash function.

    Args:
        key:    Key of userspace cache or megaflow cache subtable.
        header: Full header of packet being processed.
    Returns:
        Number for input to murmurhash function.
    """
    translations = [48, 32, 32, 8, 16, 16, 0]
    bit_length = [48, 48, 32, 32, 8, 16, 16]
    masks = [281474976710655, 281474976710655, 4294967295, 4294967295, 255, 65535, 65535]
    vital_fields = list()
    for ind in range(6, -1, -1):
        header >>= translations[ind]
        if ind in key:
            vital_fields.insert(0, header & masks[ind])
    out = 0
    for ind, field in enumerate(key):
        out = (out << bit_length[field]) | vital_fields[ind]
    return out


def subtable_lookup(subtable, header):
    """Lookups Usercache or Megaflow subtable in search for proper entry.

    Args:
        subtable: Subtable in which to seek entry.
        header:   Full header of packet being processed.
    Returns:
        None in case of miss; tuple (priority, action) for userspace cache,
        or tuple (action,) for megaflow cache.
    """
    hash_input = compute_hash_input(subtable.key, header)
    ind_in_subtable = m_hash(hash_input & subtable.mask, tab_size=1024)
    while True:
        if subtable.cache[ind_in_subtable] is None:
            return None
        else:
            if subtable.cache[ind_in_subtable][0] == hash_input & subtable.mask:
                return subtable.cache[ind_in_subtable][1:] # returns (priority, action) or (action,)
            else:
                ind_in_subtable = (ind_in_subtable + 1) % 1024
