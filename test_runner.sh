#!/bin/bash

run_5 () {
    for i in {1..10}; do
        ./ovs.py --save --profile $1 $2 $3 $4 $5 &
        sleep 1
        ./sender.py --profile $1
    done
}

prio=" --prioritysorting"
micro=" --microflow"
mega=" --megaflow"
hits=" --hits"

# Run all test ten times
run_5 1
run_5 1 $prio
run_5 1 $prio $micro

run_5 2
run_5 2 $prio
run_5 2 $prio $micro

run_5 3 $prio
run_5 3 $prio $micro
run_5 3 $prio $micro $mega

run_5 4 $prio $micro
run_5 4 $prio $micro $mega
run_5 4 $prio $micro $mega $hits

run_5 5 $prio $micro
run_5 5 $prio $micro $mega
run_5 5 $prio $micro $mega $hits
