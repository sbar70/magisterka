#!/usr/bin/env python3


def _rol32(value, translation):
    """Rotates a 32-bit value left.

    Args:
        value:       Number to rotate left.
        translation: Number of bits by which to rotate the value.
    Returns:
        Left-rotated value.
    """
    return (value << translation) & (2**32 - 1) | (value >> (32 - translation))


def m_hash(key, tab_size, seed=3594321252):
    """Computes murmurhash of given value.

    Args:
        key:      Value to compute murmurhash on.
        tab_size: Size of the hashmap in which the returned
                  value will be an index.
        seed:     Parameter to randomize the hash function.
    Returns:
        Hash value.
    """
    c1 = 0xcc9e2d51
    c2 = 0x1b873593
    r1 = 15
    r2 = 13
    m = 5
    n = 0xe6546b64

    hash = seed

    # take 32b chunks
    mask = 2**32 - 1
    while key >= mask:
        k = key & mask

        k = (k * c1) % 2**32
        k = _rol32(k, r1)
        k = (k * c2) % 2**32

        hash ^= k
        hash = _rol32(hash, r2)
        hash = (hash * m + n) % 2**32

        key >>= 32
    else:
        # process any remnants
        if key != 0:
            key = (key * c1) % 2**32
            key = _rol32(key, r1)
            key = (key * c2) % 2**32

            hash ^= key

    hash ^= 32

    hash = hash ^ (hash >> 16)
    hash = (hash * 0x85ebca6b) % 2**32
    hash = hash ^ (hash >> 13)
    hash = (hash * 0xc2b2ae35) % 2**32
    hash = hash ^ (hash >> 16)

    return hash % tab_size
