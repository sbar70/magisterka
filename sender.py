#!/usr/bin/env python3


import socket
import sys
import re
import argparse
import utils
from random import randint


def new_packet(src_2='11:11:11:11:11:11', dst_2='99:99:99:99:99:99',
               src_3='10.10.10.1', dst_3='20.20.20.1', proto='6',
               src_port='1000', dst_port='2000', message='nothing'):
    """Creates header and payload of packet to
    be sent to OvS model.

    Args:
        src_2:    Source MAC address in format
                  'ab:ab:ab:ab:ab:ab'.
        dst_2:    Destination MAC address in
                  format as above.
        src_3:    Source IP address in format
                  '192.168.0.1'.
        dst_3:    Destination IP address in
                  format as above.
        proto:    Protocol of packet in string format.
        src_port: Source port in string format.
        dst_port: Destination port in string format.
        message:  Payload of packet (max 7 characters).
    Returns:
        Packet (i.e. header and payload) in bytes format.
    """
    # src MAC
    if not re.match(r'^(?:[a-f0-9]{2}:){5}[a-f0-9]{2}$', src_2):
        raise ValueError('bad source MAC')

    # dst MAC
    if not re.match(r'^(?:[a-f0-9]{2}:){5}[a-f0-9]{2}$', dst_2):
        raise ValueError('bad destination MAC')

    # src IP
    if not re.match(r'^(?:\d{1,3}\.){3}\d{1,3}$', src_3):
        raise ValueError('bad source IP')

    # dst IP
    if not re.match(r'^(?:\d{1,3}\.){3}\d{1,3}$', dst_3):
        raise ValueError('bad destination IP')

    # protocol
    if not re.match(r'^\d+$', proto):
        raise ValueError('bad protocol number')

    # src port
    if not re.match(r'^\d+$', src_port):
        raise ValueError('bad source port')

    # src port
    if not re.match(r'^\d+$', dst_port):
        raise ValueError('bad destination port')

    # payload
    if len(message) > 7:
        raise ValueError('message must have at most 7 characters')
    elif len(message) < 7:
        # add padding
        message = (7 - len(message)) * '0' + message
    
    # conversions to numbers
    src_2 = utils.mac_to_num(src_2)
    dst_2 = utils.mac_to_num(dst_2)
    src_3 = utils.ip_to_num(src_3)
    dst_3 = utils.ip_to_num(dst_3)
    proto = int(proto)
    src_port = int(src_port)
    dst_port = int(dst_port)

    # building of header
    fields = [src_2, dst_2, src_3, dst_3, proto, src_port, dst_port]
    f_len = [48, 48, 32, 32, 8, 16, 16]
    header = 0
    for ind in range(7):
        header = (header << f_len[ind]) | fields[ind]

    # building message
    return header.to_bytes(25, byteorder='big') + str.encode(message)


parser = argparse.ArgumentParser(description='Program sending messages to OvS model')
parser.add_argument('--profile', action='store', required=True, type=int, help='Number of traffic profile to be sent')
args = parser.parse_args()


ss = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

s_address = './unix_domain_socket'

try:
    ss.connect(s_address)
except socket.error:
    print('Could not connect to the server')
    sys.exit(1)


try:
    if args.profile == 1:
        message1 = new_packet(src_2='73:f3:dd:08:11:8e', dst_2='a0:ce:ee:12:07:33', src_3='111.111.111.111', dst_3='222.222.222.222', proto='17', src_port='1000', dst_port='222', message='start')
        message2 = new_packet(src_2='73:f3:dd:08:11:8e', dst_2='a0:ce:ee:12:07:33', src_3='111.111.111.111', dst_3='222.222.222.222', proto='17', src_port='1000', dst_port='222', message='four')
        message3 = new_packet(src_2='73:f3:dd:08:11:8e', dst_2='a0:ce:ee:12:07:33', src_3='111.111.111.111', dst_3='222.222.222.222', proto='17', src_port='1000', dst_port='222', message='stop')
        ss.sendall(message1)
        for _ in range(199998):
            ss.sendall(message2)
        ss.sendall(message3)
    elif args.profile == 2:
        message1 = new_packet(src_2='83:3f:cd:88:43:ed', dst_2='45:83:33:c8:dd:89', src_3='133.133.133.133', dst_3='244.244.244.244', proto='17', src_port='333', dst_port='6349', message='start')
        message2 = new_packet(src_2='83:3f:cd:88:43:ed', dst_2='45:83:33:c8:dd:89', src_3='133.133.133.133', dst_3='244.244.244.244', proto='17', src_port='333', dst_port='6349', message='four')
        message3 = new_packet(src_2='83:3f:cd:88:43:ed', dst_2='45:83:33:c8:dd:89', src_3='133.133.133.133', dst_3='244.244.244.244', proto='17', src_port='333', dst_port='6349', message='stop')
        ss.sendall(message1)
        for _ in range(199998):
            ss.sendall(message2)
        ss.sendall(message3)
    elif args.profile == 3:
        messages = list()
        messages.append(new_packet(src_2='63:ce:98:33:ca:af', dst_2='ed:de:fa:53:99:cc', src_3='100.100.100.100', dst_3='200.200.200.200', proto='6', src_port=str(randint(1, 65535)), dst_port=str(randint(1, 65535)), message='start'))
        for _ in range(66666):
            src_port = str(randint(1, 65535))
            dst_port = str(randint(1, 65535))
            for i in range(3):
                messages.append(new_packet(src_2='63:ce:98:33:ca:af', dst_2='ed:de:fa:53:99:cc', src_3='100.100.100.100', dst_3='200.200.200.200', proto='6', src_port=src_port, dst_port=dst_port, message='six'))
        messages.append(new_packet(src_2='63:ce:98:33:ca:af', dst_2='ed:de:fa:53:99:cc', src_3='100.100.100.100', dst_3='200.200.200.200', proto='6', src_port=str(randint(1, 65535)), dst_port=str(randint(1, 65535)), message='stop'))
        for message in messages:
            ss.sendall(message)
    elif args.profile == 4:
        messages = list()
        messages.append(new_packet(src_2='53:cd:dd:08:38:2c', dst_2='87:33:1c:de:99:ac', src_3='10.10.10.10', dst_3='110.110.110.110', proto='6', src_port='1353', dst_port='9738', message='pre'))
        messages.append(new_packet(src_2='53:cd:dd:08:38:2c', dst_2='87:33:1c:de:99:ac', src_3='20.20.20.20', dst_3='120.120.120.120', proto='6', src_port='1353', dst_port='9738', message='pre'))
        messages.append(new_packet(src_2='53:cd:dd:08:38:2c', dst_2='87:33:1c:de:99:ac', src_3='30.30.30.30', dst_3='130.130.130.130', proto='6', src_port='1353', dst_port='9738', message='pre'))
        messages.append(new_packet(src_2='53:cd:dd:08:38:2c', dst_2='87:33:1c:de:99:ac', src_3='40.40.40.40', dst_3='140.140.140.140', proto='6', src_port='1353', dst_port='9738', message='pre'))
        messages.append(new_packet(src_2='53:cd:dd:08:38:2c', dst_2='87:33:1c:de:99:ac', src_3='50.50.50.50', dst_3='150.150.150.150', proto='6', src_port='1353', dst_port='9738', message='pre'))
        messages.append(new_packet(src_2='53:cd:dd:08:38:2c', dst_2='87:33:1c:de:99:ac', src_3='60.60.60.60', dst_3='160.160.160.160', proto='6', src_port='1353', dst_port='9738', message='pre'))
        messages.append(new_packet(src_2='53:cd:dd:08:38:2c', dst_2='87:33:1c:de:99:ac', src_3='70.70.70.70', dst_3='170.170.170.170', proto='6', src_port='1353', dst_port='9738', message='pre'))
        messages.append(new_packet(src_2='53:cd:dd:08:38:2c', dst_2='87:33:1c:de:99:ac', src_3='80.80.80.80', dst_3='180.180.180.180', proto='6', src_port='1353', dst_port='9738', message='pre'))
        messages.append(new_packet(src_2='53:cd:dd:08:38:2c', dst_2='87:33:1c:de:99:ac', src_3='90.90.90.90', dst_3='190.190.190.190', proto='6', src_port='1353', dst_port='9738', message='pre'))
        messages.append(new_packet(src_2='53:cd:dd:08:38:2c', dst_2='87:33:1c:de:99:ac', src_3='100.100.100.100', dst_3='200.200.200.200', proto='6', src_port='1353', dst_port='9738', message='pre'))
        messages.append(new_packet(src_2='fe:47:3b:a3:c3:88', dst_2='ae:7b:bd:97:cd:aa', src_3='10.35.238.33', dst_3='89.39.211.119', proto='73', src_port='1459', dst_port='64352', message='noneed'))
        messages.append(new_packet(src_2='78:11:ed:ee:c8:01', dst_2='98:33:7d:ee:cc:39', src_3='10.35.238.33', dst_3='89.39.211.119', proto='73', src_port='1459', dst_port='64352', message='noneed'))
        messages.append(new_packet(src_2='a8:33:c3:08:c3:50', dst_2='8e:c2:c4:07:37:bd', src_3='10.35.238.33', dst_3='89.39.211.119', proto='73', src_port='1459', dst_port='64352', message='noneed'))

        messages.append(new_packet(src_2='63:ce:98:33:ca:af', dst_2='ed:de:fa:53:99:cc', src_3='100.100.100.100', dst_3='200.200.200.200', proto='6', src_port=str(randint(1, 65535)), dst_port=str(randint(1, 65535)), message='start'))
        for _ in range(66666):
            src_port = str(randint(1, 65535))
            dst_port = str(randint(1, 65535))
            for i in range(3):
                messages.append(new_packet(src_2='63:ce:98:33:ca:af', dst_2='ed:de:fa:53:99:cc', src_3='100.100.100.100', dst_3='200.200.200.200', proto='6', src_port=src_port, dst_port=dst_port, message='six'))
        messages.append(new_packet(src_2='63:ce:98:33:ca:af', dst_2='ed:de:fa:53:99:cc', src_3='100.100.100.100', dst_3='200.200.200.200', proto='6', src_port=str(randint(1, 65535)), dst_port=str(randint(1, 65535)), message='stop'))
        for message in messages:
            ss.sendall(message)
    elif args.profile == 5:
        messages = list()
        messages.append(new_packet(src_2='fe:47:3b:a3:c3:88', dst_2='ae:7b:bd:97:cd:aa', src_3='10.35.238.33', dst_3='89.39.211.119', proto='73', src_port='1459', dst_port='64352', message='noneed'))
        messages.append(new_packet(src_2='78:11:ed:ee:c8:01', dst_2='98:33:7d:ee:cc:39', src_3='10.35.238.33', dst_3='89.39.211.119', proto='73', src_port='1459', dst_port='64352', message='noneed'))
        messages.append(new_packet(src_2='a8:33:c3:08:c3:50', dst_2='8e:c2:c4:07:37:bd', src_3='10.35.238.33', dst_3='89.39.211.119', proto='73', src_port='1459', dst_port='64352', message='noneed'))

        messages.append(new_packet(src_2='63:ce:98:33:ca:af', dst_2='ed:de:fa:53:99:cc', src_3='100.100.100.100', dst_3='200.200.200.200', proto='6', src_port=str(randint(1, 65535)), dst_port=str(randint(1, 65535)), message='start'))
        for _ in range(66666):
            w_flow = randint(0, 9)
            if w_flow == 0:
                src_3, dst_3 = '10.10.10.10', '110.110.110.110'
            elif w_flow == 1:
                src_3, dst_3 = '20.20.20.20', '120.120.120.120'
            elif w_flow == 2:
                src_3, dst_3 = '30.30.30.30', '130.130.130.130'
            elif w_flow == 3:
                src_3, dst_3 = '40.40.40.40', '140.140.140.140'
            elif w_flow == 4:
                src_3, dst_3 = '50.50.50.50', '150.150.150.150'
            elif w_flow == 5:
                src_3, dst_3 = '60.60.60.60', '160.160.160.160'
            elif w_flow == 6:
                src_3, dst_3 = '70.70.70.70', '170.170.170.170'
            elif w_flow == 7:
                src_3, dst_3 = '80.80.80.80', '180.180.180.180'
            elif w_flow == 8:
                src_3, dst_3 = '90.90.90.90', '190.190.190.190'
            elif w_flow == 9:
                src_3, dst_3 = '100.100.100.100', '200.200.200.200'
            src_port = str(randint(1, 65535))
            dst_port = str(randint(1, 65535))
            for i in range(3):
                messages.append(new_packet(src_2='63:ce:98:33:ca:af', dst_2='ed:de:fa:53:99:cc', src_3=src_3, dst_3=dst_3, proto='6', src_port=src_port, dst_port=dst_port, message='six'))
        messages.append(new_packet(src_2='63:ce:98:33:ca:af', dst_2='ed:de:fa:53:99:cc', src_3='100.100.100.100', dst_3='200.200.200.200', proto='6', src_port=str(randint(1, 65535)), dst_port=str(randint(1, 65535)), message='stop'))
        for message in messages:
            ss.sendall(message)
finally:
    ss.close()
