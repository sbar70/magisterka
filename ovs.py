#!/usr/bin/env python3


import socket
import os
import sys
import time
import argparse
import traceback
from userspace import Userspace
from cache import MegaflowCache, MicroflowCache


class CommException(Exception):
    def __init__(self, text):
        super().__init__(text)


    def __str__(self):
        return "{}".format(self.args[0])


    def __repr__(self):
        return "CommException({!r})".format(self.args[0])


parser = argparse.ArgumentParser(description='OvS with microflow and megaflow models')
parser.add_argument('--microflow', action='store_true', help='enable microflow cache')
parser.add_argument('--megaflow', action='store_true', help='enable megaflow cache')
parser.add_argument('--prioritysorting', action='store_true', help='sort subtables in userspace cache by priority')
parser.add_argument('--hits', action='store_true', help='constantly sort subtables in megaflow cache by number of hits')
parser.add_argument('--profile', action='store', type=int, required=True, help='which profile sender is sending, for logging info only')
parser.add_argument('--save', action='store_true', help='save experiment result to file')
args = parser.parse_args()

if args.hits and not args.megaflow:
    parser.error('--hits requires --megaflow')
if args.megaflow:
    processed_packets = 0 # for megaflow sorting only

# for purpose of saving in file only
_prioritysorting = ''
_megaflow = ''
_hits = ''
_microflow = ''
_profile = 'Profile ' + str(args.profile)
if args.prioritysorting:
    _prioritysorting = '--prioritysorting'
if args.megaflow:
    _megaflow = '--megaflow'
if args.hits:
    _hits = '--hits'
if args.microflow:
    _microflow = '--microflow'


userspace = Userspace()
megaflow = MegaflowCache()
microflow = MicroflowCache()

# Construcion of OvS tables
# subtable 0
userspace.add_flow(ovs_table=0, src_2='fe:47:3b:00:00:00 ff:ff:ff:00:00:00', dst_2='ae:7b:bd:97:cd:aa', priority=17, action=-1)
userspace.add_flow(ovs_table=0, src_2='fe:47:3b:00:00:00 ff:ff:ff:00:00:00', dst_2='55:73:d2:dc:ca:fd', priority=30, action=0)
userspace.add_flow(ovs_table=0, src_2='ed:d0:07:00:00:00 ff:ff:ff:00:00:00', dst_2='ab:ba:19:72:04:42', priority=24, action=-1)
# subtable 1
userspace.add_flow(src_3='87.43.52.0/24', dst_3='149.156.203.6', proto='17', src_port='476', dst_port='999', priority=28, action=-9)
userspace.add_flow(src_3='87.43.52.0/24', dst_3='149.156.203.6', proto='17', src_port='888', dst_port='999', priority=8, action=-9)
# subtable 2
userspace.add_flow(ovs_table=0, src_3='10.10.0.0/16', dst_3='110.110.110.0/24', proto='6', priority=24, action=-6)
userspace.add_flow(ovs_table=0, src_3='17.40.0.0/16', dst_3='110.110.110.0/24', proto='222', priority=10, action=-1)
userspace.add_flow(ovs_table=0, src_3='40.17.0.0/16', dst_3='110.110.110.0/24', proto='6', priority=4, action=-1)
# subtable 3
userspace.add_flow(ovs_table=0, src_3='20.20.20.0/24', dst_3='120.120.0.0/16', proto='6', priority=17, action=-6)
userspace.add_flow(ovs_table=0, src_3='96.37.63.0/24', dst_3='10.120.0.0/16', proto='99', priority=18, action=-1)
userspace.add_flow(ovs_table=0, src_3='43.153.79.0/24', dst_3='120.153.0.0/16', proto='6', priority=22, action=-1)
# subtable 4
userspace.add_flow(ovs_table=0, src_3='111.111.111.111', dst_3='222.222.222.222', proto='17', dst_port='222', priority=21, action=-4)
userspace.add_flow(ovs_table=0, src_3='68.173.99.1', dst_3='201.78.33.17', proto='6', dst_port='44', priority=20, action=-4)
userspace.add_flow(ovs_table=0, src_3='68.173.99.1', dst_3='201.78.33.17', proto='6', dst_port='687', priority=20, action=-4)
# subtable 5
userspace.add_flow(ovs_table=0, src_3='133.133.133.133', dst_3='244.244.244.244', proto='17', src_port='333', priority=4, action=-4)
userspace.add_flow(ovs_table=0, src_3='73.33.87.100', dst_3='11.80.34.88', proto='17', src_port='5333', priority=21, action=0)
userspace.add_flow(ovs_table=0, src_3='73.33.33.100', dst_3='11.70.34.88', proto='17', src_port='2343', priority=18, action=0)
userspace.add_flow(ovs_table=0, src_3='73.23.88.100', dst_3='11.80.222.88', proto='17', src_port='987', priority=1, action=0)
# subtable 6
userspace.add_flow(ovs_table=0, src_3='30.30.30.30', dst_3='130.130.130.0/24', proto='6', priority=21, action=-6)
userspace.add_flow(ovs_table=0, src_3='32.79.38.11', dst_3='99.77.223.0/24', proto='17', priority=14, action=-7)
# subtable 7
userspace.add_flow(ovs_table=0, src_3='40.40.40.40', dst_3='140.140.0.0/16', proto='6', priority=18, action=-6)
userspace.add_flow(ovs_table=0, src_3='87.73.111.98', dst_3='234.7.0.0/16', proto='6', priority=21, action=-9)
# subtable 8
userspace.add_flow(ovs_table=0, src_2='78:11:ed:ee:00:00 ff:ff:ff:ff:00:00', dst_2='98:33:7d:ee:cc:39', priority=21, action=0)
# subtable 9
userspace.add_flow(ovs_table=0, src_3='100.100.100.100', dst_3='200.200.200.200', proto='6', priority=20, action=-6)
userspace.add_flow(ovs_table=0, src_3='78.235.79.222', dst_3='33.325.87.33', proto='6', priority=18, action=0)
userspace.add_flow(ovs_table=0, src_3='132.55.97.22', dst_3='77.56.111.97', proto='6', priority=2, action=-4)
# subtable 10
userspace.add_flow(ovs_table=0, src_2='11:22:11:12:21:fa', dst_2='99:35:87:11:ac:8d', priority=18, action=0)
userspace.add_flow(ovs_table=0, src_2='11:22:11:12:21:fa', dst_2='da:da:7c:ab:b8:88', priority=17, action=0)
# subtable 11
userspace.add_flow(ovs_table=0, src_3='50.50.0.0/16', dst_3='150.150.0.0/16', proto='6', priority=7, action=-6)
userspace.add_flow(ovs_table=0, src_3='85.55.0.0/16', dst_3='12.97.0.0/16', proto='6', priority=16, action=-6)
userspace.add_flow(ovs_table=0, src_3='90.228.0.0/16', dst_3='17.112.0.0/16', proto='6', priority=7, action=-6)
# subtable 12
userspace.add_flow(ovs_table=0, src_2='97:d3:cc:38:77:00 ff:ff:ff:ff:ff:00', dst_2='88:df:fd:34:dd:00 ff:ff:ff:ff:ff:00', priority=14, action=-7)
# subtable 13
userspace.add_flow(ovs_table=0, src_3='60.60.0.0/16', dst_3='160.160.160.160', proto='6', priority=6, action=-6)
userspace.add_flow(ovs_table=0, src_3='73.88.0.0/16', dst_3='32.123.224.11', proto='17', priority=10, action=-2)
userspace.add_flow(ovs_table=0, src_3='60.88.0.0/16', dst_3='160.73.160.99', proto='6', priority=8, action=-6)
# subtable 14
userspace.add_flow(ovs_table=0, src_3='70.70.70.0/24', dst_3='170.170.170.0/24', proto='6', priority=8, action=-6)
userspace.add_flow(ovs_table=0, src_3='79.84.70.0/24', dst_3='173.180.110.0/24', proto='6', priority=1, action=-3)
# subtable 15
userspace.add_flow(ovs_table=0, src_3='80.80.80.0/24', dst_3='180.180.180.180', proto='6', priority=7, action=-6)
userspace.add_flow(ovs_table=0, src_3='33.32.11.0/24', dst_3='188.20.88.18', proto='6', priority=7, action=-6)
# subtable 16
userspace.add_flow(ovs_table=0, src_2='a8:33:c3:00:00:00 ff:ff:ff:00:00:00', dst_2='8e:c2:c4:00:00:00 ff:ff:ff:00:00:00', priority=6, action=0)
# subtable 17
userspace.add_flow(ovs_table=0, src_2='17:bd:ee:89:ac:08', dst_2='e4:ca:cc:ed:11:98', src_3='192.168.0.17', dst_3='88.123.88.33', proto='17', src_port='8888', dst_port='1788', priority=4, action=-8)
# subtable 18
userspace.add_flow(ovs_table=0, src_3='90.90.90.90', dst_3='190.0.0.0/8', proto='6', priority=2, action=-6)
userspace.add_flow(ovs_table=0, src_3='172.19.177.33', dst_3='190.0.0.0/8', proto='6', priority=1, action=-8)
userspace.add_flow(ovs_table=0, src_3='18.111.93.11', dst_3='190.0.0.0/8', proto='6', priority=1, action=-2)
# subtable 19 - default action (drop)
userspace.add_flow(ovs_table=0, priority=0, action=0)

userspace.populate_userspace_cache()
if args.prioritysorting:
    userspace.priority_sort_userspace()


s_address = './unix_domain_socket'

# If socket exists, delete it
try:
    os.unlink(s_address)
except OSError:
    pass

ss = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
ss.bind(s_address)
ss.listen()

start = None

buckets = [set(), set(), set(), set(), set(), set(), set(), set(), set(), set()]
buckets_hit = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
try:
    conn, c_address = ss.accept() # Serve one connection - experiments will be conducted in such manner
    print('ovs receiving data...')
    while True:
        data = conn.recv(32)
        header = int.from_bytes(data[:25], byteorder='big')
        payload = data[25:].decode()
        if header == 0:
            raise CommException('ERROR: No data on socket!')

        if 'start' in payload:
            start = time.time()

        if args.microflow:
            action = microflow.pkt_lookup(header) # FIRST LOOKUP, MICROFLOW CACHE
            if action is not None:
                buckets[action].add(payload)
                buckets_hit[action] += 1
                if 'stop' in payload:
                    if not start:
                        raise CommException('ERROR: Start message has not been received!')
                    stop = time.time()
                    print('Processing took {}'.format(stop - start))
                    if args.save:
                        with open('results.txt', 'a') as fh:
                            fh.write('{:9} {:17} {:10} {:6} {:11}      {}\n'.format(_profile, _prioritysorting, _megaflow,
                                        _hits, _microflow, stop - start))
                        with open('for_export.txt', 'a') as fd:
                            fd.write('{}\n'.format(stop - start))
                    break
                continue


        if args.megaflow:
            action = megaflow.pkt_lookup(header) # MIDDLE LOOKUP, MEGAFLOW CACHE
            if action is not None:
                buckets[action].add(payload)
                buckets_hit[action] += 1
                if 'stop' in payload:
                    if not start:
                        raise CommException('ERROR: Start message has not been received!')
                    stop = time.time()
                    print('Processing took {}'.format(stop - start))
                    if args.save:
                        with open('results.txt', 'a') as fh:
                            fh.write('{:9} {:17} {:10} {:6} {:11}      {}\n'.format(_profile, _prioritysorting, _megaflow,
                                        _hits, _microflow, stop - start))
                        with open('for_export.txt', 'a') as fd:
                            fd.write('{}\n'.format(stop - start))
                    break
                processed_packets += 1
                if args.microflow:
                    microflow.add_flow(header, action)
                if args.hits and processed_packets % 100 == 0:
                    megaflow.sort_by_hits() # sort each 100 megaflow hits
                continue

        action, tracked_fields = userspace.lookup(header) # LAST LOOKUP, USERSPACE

        # add entry to megaflow cache
        if args.megaflow:
            if (set(), 0) in tracked_fields:
                pass # don't insert rule - there was a default actions in userspace pipeline
            else: # there was no default action in userspace pipeline
                tracked_fields = list(zip(*tracked_fields))
                megaflow.add_flow(tracked_fields, header, action)

        # add entry to microflow cache
        if args.microflow:
            microflow.add_flow(header, action)

        # sending packet to proper bucket
        buckets[action].add(payload)
        buckets_hit[action] += 1

        if 'stop' in payload:
            if not start:
                raise CommException('ERROR: Start message has not been received!')
            stop = time.time()
            print('Processing took {}'.format(stop - start))
            if args.save:
                with open('results.txt', 'a') as fh:
                    fh.write('{:9} {:17} {:10} {:6} {:11}      {}\n'.format(_profile, _prioritysorting, _megaflow,
                                _hits, _microflow, stop - start))
                with open('for_export.txt', 'a') as fd:
                    fd.write('{}\n'.format(stop - start))
            break

    for ind, bucket in enumerate(buckets):
        print('BUCKET #{}\tHITS: {}\t{}'.format(ind, buckets_hit[ind], bucket))
except ValueError as msg:
    print(msg)
    raise
except CommException as msg:
    print(msg)
    raise
except Exception as msg:
    print(msg)
    traceback.print_exc()
    raise
finally:
    conn.close()
    print() # for the sake of prompt appearing in newline
    sys.exit(0)
